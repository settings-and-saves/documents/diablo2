# ルーン
A4Q2 Hellforge ではドロップしない `Vex` 以降のルーンを `High Rune (HR)` と呼ぶ  
HR の入手方法は下記の合成か敵からのドロップのみ

## 合成
- T: Topaz
- A: Amethyst
- S: Sapphire
- R: Ruby
- E: Emerald
- D: Diamond
### 各ルーン \* 3
El -> Eld -> Tir -> Nef -> Eth -> Ith -> Tal -> Ral -> Ort -> Thul
### 各ルーン\*3 + Chipped (欠けた) Gem
Thul (T) -> Amn (A) -> Sol (S) -> Shael (R) -> Dol (E) -> Hel (D) -> Io
### 各ルーン\*3 (Pul のみ\*2) Flawed (傷のある) Gem
Io (T) -> Lum (A) -> Ko (S) -> Fal (R) -> Lem (E) -> Pul (D) -> Um
### 各ルーン\*2 + Gem
Um (T) -> Mal (A) -> Ist (S) -> Gul (R) -> Vex (E) -> Ohm (D) -> Lo
### 各ルーン\*2 + Flawless Gem
Lo (T) -> Sur (A) -> Ber (S) -> Jah (R) -> Cham (E) -> Zod
